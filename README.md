# Card sort

## Objectif

Manier les Promises et async/await.

## Enoncé

On cherche à développer un système collaboratif de tri de paquets de carte.
- En entrée, un orchestrateur récupère un paquet mélangé (via la fonction `generateDeck()` dans _utils/decks.js_).
- Il va couper ce paquet en 4 et confier les paquets de 13 cartes à 4 trieurs de familles (la fonction exportée dans _workers/familyDispatcher.js_)
	- Leur but est simplement de séparer les cartes qui leur sont confiées en 4 tas, un tas par famille
- L'orchestrateur va ensuite récupérer ces tas, les regrouper et obtenir logiquement 4 familles séparées, contenant chacune les 13 valeurs différentes, de l'as au roi
- Il va confier ces familles aux trieurs de valeurs (la fonction exportée dans _workers/valueSorter.js_... ah tiens, il va falloir la coder) qui vont appliquer des algorithmes de tri simples pour trier les valeurs
	- Ces trieurs retourneront donc chacun leur famille triée
		- NB: Ils n'ont pas besoin de connaître leur famille, car ils sont sûrs que les 13 cartes qui leur sont confiées sont d'une unique famille, ils peuvent se concentrer sur les valeurs
		- NB2: Par contre, l'orchestrateur a besoin de savoir quelle famille il confie à quel trieur, car il devra rendre un paquet avec les familles dans un ordre précis
- A la fin de ce processus, l'orchestrateur récupère logiquement un paquet de 52 cartes triées de l'as de pique au roi de carreau (les familles étant triées dans l'ordre pique > coeur > trèfle > carreau)

### Exo 1

Compléter le trieur par valeur en appliquant un principe de recherches successives :
- En s'appuyant sur la const `values` exportée dans _utils/decks.js_, la fonction va rechercher dans son paquet l'as, puis le 2, puis le 3 etc.
- Le trieur par valeur a un travail fatiguant, il a le droit à 110 millisecondes de pause entre chaque recherche, une fonction `sleep` existe à cet effet.
- Coder l'orchestrateur (index.js) pour :
  - générer un paquet mélangé,
	- confier 4 sous-ensembles de 13 cartes à 4 _family dispatchers_
	- constituer les familles complètes mais non triées à partir de ce que rendent les _dispatchers_
	- confier les familles à 4 _value sorters_
	- récupérer les 4 familles triées et les ordonner
	- afficher le résultat (avec `console.log`, que tu peux aussi utiliser pour les étapes intermédiaires afin de voir l'avancement du processus)

### Exo 2

Les _dispatchers_ doivent consigner tout ce qu'ils font dans un journal. Avant de trier chaque carte, ils notent donc un petit message que tu as vu apparaître dans la console ("This Jack of Clubs belongs to Clubs"). Afin d'accélérer leur étape, ils sont autorisés à ne pas attendre d'avoir écrit le message pour catégoriser une carte. Attention, ils doivent quand même écrire dans le journal.

### Exo 3

Les trieurs par valeur vont maintenant appliquer un algorithme de [tri à bulles](https://fr.wikipedia.org/wiki/Tri_%C3%A0_bulles).

Le principe est de comparer la carte 1 à la carte 2 et de les inverser si elles sont dans le désordre, puis les cartes 2 et 3, 3 et 4 etc. jusqu'à comparer les cartes n-1 et n (n étant la taille du paquet de cartes), en inversant à chaque fois que c'est nécessaire.

Avec ce premier tour, le roi a normalement été "poussé" jusqu'au fond du paquet, il n'a plus besoin d'être comparé. Au tour suivant, l'algorithme reprendra donc de la carte 1 mais n'ira que jusqu'à comparer les cartes n-2 et n-1. A chaque tour, l'algorithme fera une comparaison de moins.

Et si, lors d'un tour, quelle que soit sa longueur, aucune inversion n'est faite, c'est que le paquet est trié, ça ne sert à rien de continuer.

Parce que cette tâche est plus drôle que la simple recherche, les trieurs ont le droit à seulement 65 millisecondes de pause entre chaque tour.

### Exo 4

Appliquer maintenant le principe du [tri à peigne](https://fr.wikipedia.org/wiki/Tri_%C3%A0_peigne), version améliorée du tri à bulles où les petites valeurs situées à la fin du paquet vont pouvoir plus rapidement migrer au début du paquet. Je te laisse lire ce qu'il faut changer côté implémentation.

### Exo 5

On va maintenant trier 50 paquets de cartes en même temps. Pour ça, l'orchestrateur va regrouper toute la logique de tri d'un paquet dans une fonction, qui prend le paquet en argument. Cette fonction sera naturellement async puisqu'elle fait appel à d'autres fonctions async.

L'orchestrateur va ensuite générer 50 paquets, puis lancer le tri de ces paquets en parallèle (plutôt que d'attendre le tri du paquet 1 pour lancer le tri du paquet 2). Lorsqu'un paquet est trié, il devra afficher dans la console :

```
*** SORTED DECK NUMBER x ***
First card : x(value) of x(family)
Last card : x(value) of x(family)
```

Logiquement, ces cartes devraient toujours être _Ace of Spades_ et _King of Diamonds_ mais affiche le message dynamiquement quand même, ça te permettra de t'en assurer.

### Exo 6

Le tri de 50 paquets prend énormément de ressources, car il y a en fait 50 * 4 = 200 fonctions _familyDispatcher_ qui tournent en même temps, puis cela devient progressivement (au fur et à mesure que les _dispatchers_ se terminent) 200 fonctions _valueSorter_.

Réfléchis à une solution pour n'avoir que 8 workers de chaque type en même temps. Cela permettra de lancer le tri des paquets 1 et 2, mais pour le troisième, il faudra attendre que des _dispatchers_ se libèrent. Attention à conserver l'info du numéro de paquet, car les dispatchers ont une vitesse variable. Ainsi, un dispatcher pourrait, par exemple, trier une partie du paquet 8 pendant que son voisin termine le tri d'une partie du paquet 3.

### Exo 7

Les messages affichés par les _dispatchers_ sont très brouillons, car ils apparaissent "longtemps" (jusqu'à 150 millisecondes) après l'action qu'ils décrivent. Avec l'aide de `Promise.all` ([la doc est ici](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Promise/all)), fais en sorte qu'un _dispatcher_ retourne ses familles triées une fois que tous ses messages ont été écrits dans le journal.