const families = ['Spades', 'Hearts', 'Clubs', 'Diamonds']
const values = ['Ace', 2, 3, 4, 5, 6, 7, 8, 9, 10, 'Jack', 'Queen', 'King']

const generateDeck = () => {
	let deck = []
	families.forEach((family) => {
		values.forEach((value) => {
			deck.push({ family, value })
		})
	})

	for (let rep = 0; rep < 17; rep++) {
		deck = deck.sort(_ => Math.random() - .5)
		if ([2, 11, 13].includes(rep)) deck = deck.reverse()
	}

	return deck
}

module.exports = {
	families,
	values,
	generateDeck,
}
