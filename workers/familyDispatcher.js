const logCard = async (card) => {
	console.log(await new Promise((r) => setTimeout(r, Math.random() * 150, `This ${card.value} of ${card.family} belongs to ${card.family}`)))
}

const { families } = require('../utils/decks')

module.exports = async (cards) => {
	// ce worker récupère 13 cartes au hasard, il va retourner un objet avec 4 propriétés correspondant aux 4 familles
	// ces 4 props sont des arrays qui contiennent les cartes de la famille correspondante

	const result = Object.fromEntries(families.map((f) => [f, []]))

	for (let card of cards) {
		await logCard(card).then(_ => result[card.family].push(card))
	}

	console.log('Okay, done !')
	return result
}
